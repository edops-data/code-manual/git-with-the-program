# I Demand To Speak With the Branch Manager

![I Demand To Speak To Your Manager](https://memegenerator.net/img/instances/69231796.jpg)

> To demonstrate how branches work, we're going to play a game of Battleship.

## I. Clone the Battleship Repo

In **Git Bash** or **Anaconda Prompt**, type 

  `git clone https://gitlab.com/edops-data/code-manual/battleship` and press **Enter**.

## II. Assemble into teams and name them.

Though, interestingly, you don't have to be co-loacated.  Slack may help.

You will, however, have to decide on a team name. Constraints:

- Team name must be **one word**, lowercase.
- Name cannot be a git reserved word.

## III. Create a branch for your team.

Once you've decided on a team name, you will create a branch for your team so that you can set up your battlefield in private.

### Pick **one** person to populate the battlefield.

Have that person enter **Git Bash** or **Anaconda Prompt** and type

  `git checkout -b <team>`, where `<team>` is your team name, and press **Enter**.

  You should see something like

  ```bash
  $ git checkout -b dearleader
  Switched to a new branch 'dearleader'
  ```

  Then:

  - type `git add .` and press **Enter**
  - type `git commit -m "Create team"` and press **Enter**
  - type `git push` and press **Enter**

  With luck, you have pushed your new branch to GitLab.

> **Note for the nerds:**
>
> To continue the chain metaphor from `Hello World`, the command `git checkout` tells Git to switch chains (i.e. branches).  The `-b` flag tells Git that you're creating a new branch, which begins as a carbon copy of whatever branch you were coming from.
>
> Once a branch is created, it remains forever separate from the other branches, with its own edit chain.  To integrate a branch's changes back into the `master` chain, you use a `merge` request.
>
>More on those later.

### Have all team members switch to the branch

Everybody else needs to type:
- `git pull`
- `git checkout <team>` where `<team>` is the team name.

  You should see something like

  ```bash
  $ git checkout -b dearleader
  Switched to a new branch 'dearleader'
  ```

## III. Populate your battlefield

The fun part about this is that you can do it **together**!

### Pick **one person** to populate the battlefield

This need not be the person who created the branch.

1. Have the designated person open `battlefield.md` in a code editor and update the table using the appropriate letters for each ship.  You can use Slack (or whispers) to decide on the locations.

  > Don't worry about keeping the table structure intact! As you'll notice in the file, Markdown is surprisingly understanding when it comes to spacing.

2. Once the battlefield is complete, that person should `git add/commit/push` to the repo.

### Pull the new battlefield

All other team members should now type `git pull` and press **Enter** to pull the newly-formed battlefield

## IV. (Optional) Sumbmit a Merge Request to create a master battlefield

If you want spectators (or an official) to view the combined board, you can accomplish this with a **merge**.  The official (Adam) will perform the merge, but first you must place a bureaucratic-sounding **merge request**.

Delegate **one person** to perform this.

1. After you've pushed your battlefield, navigate to https://gitlab.com/edops-data/code-manual/battleship.

2. In the left-hand menu, navigate to **Repository** | **Branches**.

3. Find your branch and click **Merge Request**.

4. You can leave the description blank.  Assign to **Adam** and click **Submit**

> Once both teams have submitted their merge requests, a master map will be created in a new branch.  Don't cheat!

## V. Game On!

Enjoy firing missiles and sinking stuff!

## VI. What should I have written in the Description box?

Sounds like you're interested in writing a [changelog](changelog.md).