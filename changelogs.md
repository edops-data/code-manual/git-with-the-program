# ChangeLogs: Better than Bad - They're Good!

![](https://cdn.shopify.com/s/files/1/0301/0501/products/log_ren_and_stimpy_t_shirt_textual_tees.png?v=1513121406)

Full disclosure: this isn't a lesson, but more of a reference.

## I. Ways to Document Changes

### A. Commit messages

Commits require an accompanying message. This is accomplished either with the `git commit -m` flag (command line) or the _Commit Message_ box (Atom, VS Code).

> Traditionally, the first line of the commit message is capped at 50 characters.  For the most part, you should stick to this standard.
>
> The resulting messages will be terse, but also concise.

The 50-character limit is also a nudge to commit your changes in **small, focused bundles**.

### B. Changelog

Unlike commits, which document small changes that may not necessarily add or repair functionality, the changelog documents completed alterations that the user (or other target audience) would actually notice.

The changelog gets updated at every release stage (major, minor, or patch) as defined by [Semantic Versioning](semver.org) and usually corresponds with the commit being tagged.

## II. Rules for Documenting Changes

### A. Commit messages

Commit messages should take the form `type: message`, as in the following examples:

- `feat: filter students by At-Risk`
- `fix: Cast SortOrder to INT to fix ordering`
- `docs: Update README`
- `style: Standardize 2-space indents`

The **type** should be one of the seven enumerated types from the [Udacity Git Style Guide](http://udacity.github.io/git-styleguide/), ordered by frequency:

| Type | Description |
| ---: | --- |
| **feat** | New feature |
| **fix** | Bug fix |
| **docs** | Change to documentation |
| **style** | Formatting with no code change (e.g. semicolons, indents) |
| **refactor** | Refactoring production code |
| **test** | Adding tests with no change to production code |
| **chore** | Updating build tasks, package manager configs, etc; no production code change |

> **NB:** If your commit is the result of a JIRA task, you should include that task number in the commit message.

As mentioned earlier, the total commit message should, when possible, be **under 50 characters**.  More detailed descriptions belong in the

- Changelog
- Release Notes, or
- Documentation


### B. Changelog

The changelog should reside in one of two places:

1. At the end of `README.md`
2. In a top-level file named `CHANGELOG.md`

In both cases, it should take the form specified at keepachangelog.com:

- Reverse chronology (most recent at top)
- Level 2 heading with the date (and version number where applicable)
- Level 3 heading with the change type
- Bulleted list of changes

By way of example from keepachangelog.com:

```markdown
## [0.0.7] - 2015-02-16
### Added
- Link, and make it obvious that date format is ISO 8601.

### Changed
- Clarified the section on "Is there a standard change log format?".

### Fixed
- Fix Markdown links to tag comparison URL with footnote-style links.
```

The **type** of change should be one of the following:

| Type | Description |
| ---: | --- |
| **Added**| New feature |
| **Changed** | Change to existing functionality |
| **Deprecated** | Soon-to-be-removed features |
| **Removed** | Removed features |
| **Fixed** | Bug fixes |
| **Security** | Known or patched vulnerabilities |
| **Unreleased** | Changes to the code in anticipation of upcoming features |

More details on changelog protocols are in the Style Guide.