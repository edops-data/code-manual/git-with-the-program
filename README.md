# Git with the Program

Git training for EdOps Data Specialists

## Part 0: Preparation

### A. Install Git

Two ways to do this, at least one of which you may already have.

#### Git Bash

Navigate to https://git-scm.com/downloads, and install Git.

### Anaconda Prompt

Navigate to https://www.anaconda.com/distribution/ and install the _Python 3.7_ version.

> **Note:** Git Bash uses Linux shell commands, while Anaconda uses the Windows shell. For this training the only shell command we'll use is `cd`, which is the same in both environments, but you should still be aware.

---

### B. Install a Code Editor

Any text editor will be fine, since source code is stored as plain text.  However, there are two popular code editors with built-in support for Git. Built-in support = lazy user = happy user.

> Both are nearly the same (VS Code is a skinned copy of Atom); the main distinction is that Atom has a cleaner interface, while VS Code has better integrations (since they're curated by Microsoft).
>
> VS Code has far better preview ability, particularly for Markdown, which is why I'm writing this in VS Code now.
>
> VS Code's panels for file navigation and Git, however, are a bit crap, which is why every time I need to commit this repo, I switch back to Atom. Seriously.

#### Atom

Navigate to https://atom.io, download, and install.

#### Visual Studio Code

Navigate to https://code.visualstudio.com/, download, and install.

---

### C. Refresh your Git Basics (Hello World Exercise)

If you're comfortable with the basic Git commands, namely `git clone`/`pull`/`add`/`push`/`commit`, then you can skip this section.  Otherwise, it takes ~10 minutes and provides some helpful familiarity.

**Open the [Hello World](https://gitlab.com/edops-data/code-manual/hello-world) exercise**

---

## Part 1: When Commits Collide

View the [materials](collisions.md)

---

## Part 2: I Demand to Speak with the Branch Manager

View the [materials](branches.md)

---

## Part 3: ChangeLogs: Better Than Bad - They're Good!

View the [materials](changelogs.md)