# When Commits Collide

![When Worlds Collide](https://m.media-amazon.com/images/M/MV5BZjNlYzI1NWItMmZiMS00Mzc2LWJiNGMtODkwM2YzODBhNzE3XkEyXkFqcGdeQXVyNjQzNDI3NzY@._V1_.jpg)

## I. Clone the `Hello World` Repository (again)

> **NB:** If you cloned the `Hello World` repo to do the pre-work, you'll need to delete the folder.

1. In either **Git Bash** or **Anaconda Prompt**, navigate to the folder where you want your repo to live.

    > Don't create the `hello-world` folder itself! That folder will be created when you clone the repo.

2. Clone the repository by typing

    `git clone https://gitlab.com/edops-data/code-manual/hello-world`

    You should see something along the lines of

    ```bash
    $ git clone https://gitlab.com/edops-data/code-manual/hello-world
    Cloning into 'hello-world'...
    warning: redirecting to https://gitlab.com/edops-data/code-manual/hello-world.git/
    remote: Enumerating objects: 6, done.
    remote: Counting objects: 100% (6/6), done.
    remote: Compressing objects: 100% (3/3), done.
    remote: Total 6 (delta 0), reused 0 (delta 0)
    Unpacking objects: 100% (6/6), done.
    ```

## II. Make some changes

### A. Without a fancy code editor

1. In your favorite text editor, open `20wordbio.md` and create your 20-word biography.
2. Save the file.

### B. With a fancy code editor

Both [Atom](https://atom.io) and [Visual Studio Code](https://code.visualstudio.com) have Git support and will let you monitor changes and diffs from within the editor.

1. Navigate to

    **File** | **Add Folder to Workspace** (VS Code) _or_
    **File** | **Add Project Folder** (Atom)

2. Navigate to the `hello-world` folder and click **Open**.

3. Open `20wordbio.md` and create your 20-word biography.

4. Save the file.  You'll notice that your editor has picked up on the changes and has marked the file accordingly.

## III. Commit but **do not push** your changes

### A. Without a fancy code editor

1. **Stage** your changes by typing

    `git add .` (include the period) and press **Enter**.

    There won't be any response, so to confirm that something happened, type

    `git status` and press **Enter**.

    You should see something like
    ```bash
    $ git status
    On branch master
    Your branch is up to date with 'origin/master'.

    Changes to be committed:
      (use "git reset HEAD <file>..." to unstage)

            modified:   20wordbio.md
    ```

    > You just told Git to put a marker at your last change. Nothing has changed on the server yet - this is all local.

2. **Commit** your changes by typing

    `git commit -m "Add 20-word bio"` (or use your own message) and pressing **Enter**.

    You should see something like

    ```bash
    $ git commit -m "Change 20-word bio"
    [master 239813e] Change 20-word bio
    1 file changed, 2 insertions(+)
    ```

    > You just told Git to package all of the changes you staged into a single _commit_ that marks those changes as a single object.  This is **still** local - nothing is on the server yet!

3. Stop here. Don't push your changes... yet

### B. With a fancy code editor

#### VS Code

1. In the left-hand navbar, click the Git icon (looks like a branching path).

2. Find the section titled `HELLO-WORLD (GIT)`. If you hover over the title, you'll see 3 hotdog dots and a checkmark. We'll use those in a moment.

3.  **Stage** your changes by clicking on the hotdog dots and choosing **Stage all changes**.

    > You just told Git to put a marker at your last change. Nothing has changed on the server yet - this is all local.

4.  Enter your **commit message** in the box and press **Enter** to save.

5.  **Commit** your changes by clicking the checkmark in the title bar.

    > You just told Git to package all of the changes you staged into a single _commit_ that marks those changes as a single object.  This is **still** local - nothing is on the server yet!

6.  Stop here. Don't push your changes... yet

#### Atom

1. Display the **Git Pane** by navigating to **View** | **Toggle Git Tab**.

2. Under **Unstaged Changes** you should see `20wordbio.md`.  If you don't, make sure you saved your file.  If you _still_ don't, make sure that the notebook icon for `hello-world` is selected in the **Project** pane (on the left).

3. **Stage** your changes by clicking **Stage All** in the Git pane.

    > You just told Git to put a marker at your last change. Nothing has changed on the server yet - this is all local.

4. Enter your **commit message** in the box.

5. **Commit** your changes by clicking **Commit to Master**.

    > You just told Git to package all of the changes you staged into a single _commit_ that marks those changes as a single object.  This is **still** local - nothing is on the server yet!

6. Stop here. Don't push your changes... yet

## III. Create a Collision

### A. _Attempt_ to push your changes

While you were working (assuming this is a live session), I pushed my own change to the repo, replacing all the example 20-word biographies with Donald Trump tweets about celebrities.

1. **Push** your changes to the repo using your preferred method.

    If you pushed using the command prompt, you'll see something like

    ```bash
    $ git push
    warning: redirecting to https://gitlab.com/alabay_edops/hello-adam.git/
    To https://gitlab.com/alabay_edops/hello-adam
    ! [rejected]        master -> master (fetch first)
    error: failed to push some refs to 'https://gitlab.com/alabay_edops/hello-adam'
    hint: Updates were rejected because the remote contains work that you do
    hint: not have locally. This is usually caused by another repository pushing
    hint: to the same ref. You may want to first integrate the remote changes
    hint: (e.g., 'git pull ...') before pushing again.
    hint: See the 'Note about fast-forwards' in 'git push --help' for details.
    ```

    If you pushed using a code editor, it probably skipped this part and took you to the next step (skip to [Reconcile with a merge](#b-reconcile-with-a-merge-optional))

This means that your commit is now _behind the HEAD_, which is a fancy way of saying that files you thought you were changing are no longer the official, correct files.

> **Note for the nerds:**
>
> In Git, everything is tracked as a chain of changes. When you commit, you create a new link in that chain which consists of all the changes made since the last link.
>
>When you pushed your changes, Git was expecting to see a certain link at the end of the chain, but was disappointed when it found mine instead. Hence the error.



### B. Reconcile with a `merge` (optional)

This step is optional, and only if you're interested.  The point of the exercise was to impress that:

- multiple commits to the same branch can cause collisions
- collisions are messy to repair
- ergo, avoid collisions.

> Everything hereafter is best done with a code editor and **not** to be attempted via command line or a regular text editor.  It gets real messy real fast.
>
>(If you don't believe me, look at the commit logs for this very project.  It's hellish.)

In your editor - you may have to click around - you will see something like


```diff
-Taken (and occasionally adapted) from https://businessesgrow.com/2011/09/21/20-of-the-worlds-most-clever-twitter-bios/
+### [@RealDonaldTrump](https://twitter.com/realdonaldtrump)

-### [@MichaelACaruso](https://twitter.com/michaelacaruso)
+> Washed up psycho @BetteMidler was forced to apologize for a statement she attributed to me that turned out to be totally fabricated by her in order to make “your great president” look really bad. She got caught, just like the Fake News Media gets caught. A sick scammer!

-> I’ve learned I don’t know anything.  Have also learned that people will pay for what I know.  Life is good.
```

For each of these changes, you will need to pick which you want to keep.  When everything consists of happy paragraphs this is bad enough, but you can imagine the nightmare when the changes happen mid-line, or, heaven forbid, contain formatting.


```diff
1. In GitLab, navigate to your personal profile at `https://gitlab.com/<username>`.
2. Navigate to **Projects** | **Your Projects**.
3. Click **New Project**.
-4. Name the project `Hello World`. Or don't - it's your project. Go nuts. But we're assuming that its folder is `hello-world`
+4. Name the project `Hello World`. Or don't - it's your project. Go nuts.
5. Keep the visibility to `Private`.
    > This happy little ~~tree~~ repository will be our little secret. -Bob Ross
6. Click **Create Project**.
```

---

Once you have reconciled the changes, you can commit.  Or, you can accept a **merge** where the software picks the most-recent lines from both versions and adopts the hybrid file as the new normal.

Alternatively, you can admit defeat and ditch your changes by using the **nuclear option**:

`git reset --hard HEAD`

which, as the name suggests, liquidates all of your changes and adopts the top link in the chain as the new normal.

## IV. The Moral of the Story

Collisions suck.  Branches and merge requests prevent collisions.  Use [branches](./branches.md).